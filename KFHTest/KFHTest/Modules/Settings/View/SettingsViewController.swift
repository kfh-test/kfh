//
//  SettingsViewController.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var TableView_Details: UITableView!
    private let cellIdentifier = "TableViewCell"
    
    private let settingsDataSource = SettingsDataSource()
    private var items:[SettingsDataModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        prepareData()
        initializeTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.navigationItem.title = "Settings"
    }
    
    private func buildUI() {
        tabBarController?.navigationItem.hidesBackButton = true
    }
    
    private func prepareData() {
        items = settingsDataSource.prepareData()
    }
    
    private func initializeTableView() {
        TableView_Details.delegate = self
        TableView_Details.dataSource = self
        
        TableView_Details.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        TableView_Details.tableFooterView = UIView(frame: .zero)
        TableView_Details.separatorStyle = .singleLine
        TableView_Details.delaysContentTouches = false
    }
    
    private func showLogoutAlert() {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] action in
            NetworkManager.shared.clearCache()
            UserDefaults.standard.removeObject(forKey: Urls.WORKSPACE_KEY)
            self?.navigationController?.popToRootViewController(animated: true)
        })
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == (items?.count ?? 0) - 1 {
            showLogoutAlert()
        }
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TableViewCell, let item = items?[indexPath.row] {
            cell.configureCell(data: item)
            if item.isLogoutRow {
                cell.selectionStyle = .default
            }
            else {
                cell.selectionStyle = .none
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
