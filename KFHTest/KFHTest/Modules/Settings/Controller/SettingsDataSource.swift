//
//  SettingsDataSource.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class SettingsDataSource {
    func prepareData() -> [SettingsDataModel] {
        var retVal = [SettingsDataModel]()
        
        let mirrored_object = Mirror(reflecting: User.sharedInstance)
        for (_, attr) in mirrored_object.children.enumerated() {
            if let property_name = attr.label as String? {
                if let stringValue = attr.value as? String {
                    retVal.append(SettingsDataModel(title: property_name, value: stringValue))
                }
                else if let boolValue = attr.value as? Bool  {
                    retVal.append(SettingsDataModel(title: property_name, value: boolValue.description))
                }
            }
        }
        if let href = User.sharedInstance.links?.hooks?.href {
            retVal.append(SettingsDataModel(title: "hooks", value: href))
        }
        if let href = User.sharedInstance.links?.avatar?.href {
            retVal.append(SettingsDataModel(title: "avatar", value: href))
        }
        if let href = User.sharedInstance.links?.selfLink?.href {
            retVal.append(SettingsDataModel(title: "self", value: href))
        }
        if let href = User.sharedInstance.links?.repositories?.href {
            retVal.append(SettingsDataModel(title: "repositories", value: href))
        }
        if let href = User.sharedInstance.links?.html?.href {
            retVal.append(SettingsDataModel(title: "html", value: href))
        }
        if let href = User.sharedInstance.links?.snippets?.href {
            retVal.append(SettingsDataModel(title: "snippets", value: href))
        }
        
        let logoutRow = SettingsDataModel(title: "Logout", value: "")
        logoutRow.isLogoutRow = true
        retVal.append(logoutRow)
        
        return retVal
    }
}

class SettingsDataModel:CellDataModel {
    var isLogoutRow = false
}
