//
//  User.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class User:Codable {
    static var sharedInstance = User()
    
    var username:String?
    var has_2fa_enabled:String?
    var display_name:String?
    var account_id:String?
    var nickname:String?
    var created_on:String?
    var is_staff:Bool?
    var location:String?
    var account_status:String?
    var type:String?
    var uuid:String?
    var links:Links?
}

class Links:Codable {
    var hooks:Reference?
    var selfLink:Reference?
    var repositories:Reference?
    var html:Reference?
    var avatar:Reference?
    var snippets:Reference?
    
    enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case hooks, repositories, html, avatar, snippets
    }
}

class Reference:Codable {
    var href:String?
}
