//
//  LoginDataSource.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

protocol LoginProtocol:class {
    func success()
    func error(message:String?)
}

class LoginDataSource {
    
    private weak var delegate:LoginProtocol?
    
    func setDelegate(delegate:LoginProtocol) {
        self.delegate = delegate
    }
    
    func login(username:String, password:String) {
        NetworkManager.shared.setCredentials(username: username, password: password)
        NetworkManager.shared.dataTask(serviceURL: Urls.AUTHORIZE, httpMethod: .get, parameters: nil) { [weak self] (response, error, status) in
            if response != nil, let responseData = response as? Data {
                let decoder = JSONDecoder()
                User.sharedInstance = try! decoder.decode(User.self, from: responseData)
                
                self?.delegate?.success()
            }
            else if error != nil {
                self?.delegate?.error(message: error?.localizedDescription)
            }
            else if let statusCode = status {
                self?.delegate?.error(message: "Status returned: \(statusCode)")
            }
        }
    }
}
