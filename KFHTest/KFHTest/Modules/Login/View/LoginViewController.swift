//
//  LoginViewController.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var TextField_Username: UITextField!
    @IBOutlet weak var TextField_Password: UITextField!
    @IBOutlet weak var TextField_Workspace: UITextField!
    
    @IBOutlet weak var ErrorLabel_Username: UILabel!
    @IBOutlet weak var ErrorLabel_Password: UILabel!
    @IBOutlet weak var ErrorLabel_Workspace: UILabel!
    
    @IBOutlet weak var Button_Login: UIButton!
    
    let loginDataSource = LoginDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        buildUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearData()
    }
    
    private func initialize() {
        TextField_Username.delegate = self
        TextField_Password.delegate = self
        TextField_Workspace.delegate = self
        loginDataSource.setDelegate(delegate: self)
    }
    
    private func buildUI() {
        title = "Login"
    }
    
    private func validate() -> Bool {
        var retVal = true
        if TextField_Username.isEmpty {
            retVal = false
            ErrorLabel_Username.text = "Please enter username"
        }
        else {
            ErrorLabel_Username.clear()
        }
        if TextField_Password.isEmpty {
            retVal = false
            ErrorLabel_Password.text = "Please enter password"
        }
        else {
            ErrorLabel_Password.clear()
        }
        if TextField_Workspace.isEmpty {
            retVal = false
            ErrorLabel_Workspace.text = "Please enter workspace"
        }
        else {
            ErrorLabel_Workspace.clear()
        }
        return retVal
    }
    
    private func hideKeyboard() {
        view.endEditing(true)
    }
    
    private func clearData() {
        TextField_Username.clear()
        TextField_Password.clear()
        TextField_Workspace.clear()
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        hideKeyboard()
        guard validate(), let username = TextField_Username.text, let password = TextField_Password.text, let workspace = TextField_Workspace.text else {
            return
        }
        UserDefaults.standard.set(workspace, forKey: Urls.WORKSPACE_KEY)
        LoadingStart()
        loginDataSource.login(username: username, password: password)
    }
    
    @IBAction func gestureRecognizerTapped(_ sender: Any) {
        hideKeyboard()
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == TextField_Username {
            TextField_Password.becomeFirstResponder()
        }
        else if textField == TextField_Password {
            hideKeyboard()
        }
        return true
    }
}

extension LoginViewController: LoginProtocol {
    func success() {
        LoadingStop { [weak self] in
            self?.performSegue(withIdentifier: "login_success", sender: self)
        }
    }
    
    func error(message: String?) {
        LoadingStop { [weak self] in
            self?.showToast(message: message ?? "")
        }
    }
}

