//
//  RepositoryDetailsDataSource.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class RepositoryDetailsDataSource {
    func prepareData(repositoryDetails:Values) -> [CellDataModel] {
        var retVal = [CellDataModel]()
        
        if let scm = repositoryDetails.scm {
            retVal.append(CellDataModel(title: "scm", value: scm))
        }
        if let website = repositoryDetails.website {
            retVal.append(CellDataModel(title: "website", value: website))
        }
        if let has_wiki = repositoryDetails.has_wiki {
            retVal.append(CellDataModel(title: "has_wiki", value: has_wiki.description))
        }
        if let uuid = repositoryDetails.uuid {
            retVal.append(CellDataModel(title: "uuid", value: uuid))
        }
        if let fork_policy = repositoryDetails.fork_policy {
            retVal.append(CellDataModel(title: "fork_policy", value: fork_policy))
        }
        if let full_name = repositoryDetails.full_name {
            retVal.append(CellDataModel(title: "full_name", value: full_name))
        }
        if let name = repositoryDetails.name {
            retVal.append(CellDataModel(title: "name", value: name))
        }
        if let language = repositoryDetails.language {
            retVal.append(CellDataModel(title: "language", value: language))
        }
        if let created_on = repositoryDetails.created_on {
            retVal.append(CellDataModel(title: "created_on", value: created_on))
        }
        if let has_issues = repositoryDetails.has_issues {
            retVal.append(CellDataModel(title: "has_issues", value: has_issues.description))
        }
        if let updated_on = repositoryDetails.updated_on {
            retVal.append(CellDataModel(title: "updated_on", value: updated_on))
        }
        if let size = repositoryDetails.size {
            retVal.append(CellDataModel(title: "size", value: size.description))
        }
        if let type = repositoryDetails.type {
            retVal.append(CellDataModel(title: "type", value: type))
        }
        if let slug = repositoryDetails.slug {
            retVal.append(CellDataModel(title: "slug", value: slug))
        }
        if let is_private = repositoryDetails.is_private {
            retVal.append(CellDataModel(title: "is_private", value: is_private.description))
        }
        if let description = repositoryDetails.description {
            retVal.append(CellDataModel(title: "description", value: description))
        }
        if let type = repositoryDetails.type {
            retVal.append(CellDataModel(title: "type", value: type))
        }
        if let type = repositoryDetails.project?.type {
            retVal.append(CellDataModel(title: "project_type", value: type))
        }
        if let name = repositoryDetails.project?.name {
            retVal.append(CellDataModel(title: "project_name", value: name))
        }
        if let key = repositoryDetails.project?.key {
            retVal.append(CellDataModel(title: "project_key", value: key))
        }
        if let uuid = repositoryDetails.project?.uuid {
            retVal.append(CellDataModel(title: "project_uuid", value: uuid))
        }
        if let type = repositoryDetails.mainbranch?.type {
            retVal.append(CellDataModel(title: "mainbranch_type", value: type))
        }
        if let name = repositoryDetails.mainbranch?.name {
            retVal.append(CellDataModel(title: "mainbranch_name", value: name))
        }
        if let type = repositoryDetails.workspace?.type {
            retVal.append(CellDataModel(title: "workspace_type", value: type))
        }
        if let name = repositoryDetails.workspace?.name {
            retVal.append(CellDataModel(title: "workspace_name", value: name))
        }
        if let key = repositoryDetails.workspace?.slug {
            retVal.append(CellDataModel(title: "workspace_slug", value: key))
        }
        if let uuid = repositoryDetails.project?.uuid {
            retVal.append(CellDataModel(title: "workspace_uuid", value: uuid))
        }
        if let type = repositoryDetails.owner?.type {
            retVal.append(CellDataModel(title: "owner_type", value: type))
        }
        if let username = repositoryDetails.owner?.username {
            retVal.append(CellDataModel(title: "owner_username", value: username))
        }
        if let display_name = repositoryDetails.owner?.display_name {
            retVal.append(CellDataModel(title: "owner_display_name", value: display_name))
        }
        if let uuid = repositoryDetails.owner?.uuid {
            retVal.append(CellDataModel(title: "owner_uuid", value: uuid))
        }
        return retVal
    }
}
