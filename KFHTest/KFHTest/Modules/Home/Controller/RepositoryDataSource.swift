//
//  RepositoryDataSource.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

protocol RepositoryProtocol:AnyObject {
    func success(repositories:Repositories?)
    func error(message:String?)
}

class RepositoryDataSource {
    
    private weak var delegate:RepositoryProtocol?
    
    func setDelegate(delegate:RepositoryProtocol) {
        self.delegate = delegate
    }
    
    func fetchRepositories() {
        NetworkManager.shared.dataTask(serviceURL: Urls.GET_REPOSITORIES, httpMethod: .get, parameters: nil) { [weak self] (response, error, status) in
            if response != nil, let responseData = response as? Data {
                let decoder = JSONDecoder()
                let repositories = try! decoder.decode(Repositories.self, from: responseData)
                self?.delegate?.success(repositories: repositories)
            }
            else if error != nil {
                self?.delegate?.error(message: error?.localizedDescription)
            }
            else if let statusCode = status {
                self?.delegate?.error(message: "Status returned: \(statusCode)")
            }
        }
    }
}
