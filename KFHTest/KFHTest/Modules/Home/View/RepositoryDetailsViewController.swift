//
//  RepositoryDetailsViewController.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import UIKit

class RepositoryDetailsViewController: UIViewController {
    
    @IBOutlet weak var TableView_Details: UITableView!
    private let cellIdentifier = "TableViewCell"
    
    private let dataSource = RepositoryDetailsDataSource()
    private var items:[CellDataModel]?
    
    var repositoryDetails:Values?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        prepareData()
        initializeTableView()
    }
    
    func initialize() {
        title = repositoryDetails?.name
    }
    
    private func prepareData() {
        if let details = repositoryDetails {
            items = dataSource.prepareData(repositoryDetails: details)
        }
    }
    
    private func initializeTableView() {
        TableView_Details.dataSource = self
        
        TableView_Details.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        TableView_Details.tableFooterView = UIView(frame: .zero)
        TableView_Details.separatorStyle = .singleLine
        TableView_Details.delaysContentTouches = false
    }
}

extension RepositoryDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TableViewCell, let item = items?[indexPath.row] {
            cell.configureCell(data: item)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
