//
//  RepositoriesViewController.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var TableView_Repositories: UITableView!
    
    @IBOutlet weak var Label_NoData: UILabel!
    
    private let cellIdentifier = "RepositoryListCell"
    
    let repositoriesDataSource = RepositoryDataSource()
    
    var data:Repositories?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        fetchData()
        buildUI()
        initializeTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.navigationItem.title = "Home"
    }
    
    private func initialize() {
        repositoriesDataSource.setDelegate(delegate: self)
    }
    
    private func buildUI() {
        tabBarController?.navigationItem.hidesBackButton = true
    }
    
    private func fetchData() {
        LoadingStart()
        repositoriesDataSource.fetchRepositories()
    }
    
    private func initializeTableView() {
        TableView_Repositories.delegate = self
        TableView_Repositories.dataSource = self
        TableView_Repositories.tableFooterView = UIView(frame: .zero)
        TableView_Repositories.separatorStyle = .singleLine
        TableView_Repositories.delaysContentTouches = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "repository_details", let indexPath = sender as? IndexPath, let detailsVC = segue.destination as? RepositoryDetailsViewController, let details = data?.values?[indexPath.row] {
            detailsVC.repositoryDetails = details
        }
    }
}

extension HomeViewController: RepositoryProtocol {
    func success(repositories:Repositories?) {
        LoadingStop { [weak self] in
            self?.data = repositories
            self?.TableView_Repositories.reloadData()
            if self?.data?.values?.count == 0 {
                self?.Label_NoData.isHidden = false
            }
            else {
                self?.Label_NoData.isHidden = true
            }
        }
    }
    
    func error(message: String?) {
        LoadingStop { [weak self] in
            self?.showToast(message: message ?? "")
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "repository_details", sender: indexPath)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.values?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: cellIdentifier)
        }
        if let dataValue = data?.values?[indexPath.row] {
            cell?.textLabel!.text = dataValue.name
        }
        cell?.textLabel?.numberOfLines = 0
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
