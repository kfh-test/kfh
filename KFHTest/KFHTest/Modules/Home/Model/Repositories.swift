//
//  Repositories.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class Repositories:Codable {
    var pagelen:Int?
    var page:Int?
    var size:Int?
    var values:[Values]?
}

class Values:Codable {
    var scm:String?
    var website:String?
    var has_wiki:Bool?
    var uuid:String?
    var fork_policy:String?
    var full_name:String?
    var name:String?
    var language:String?
    var created_on:String?
    var has_issues:Bool?
    var updated_on:String?
    var size:Int?
    var type:String?
    var slug:String?
    var is_private:Bool?
    var description:String?
    var links:RepoLinks?
    var project:Project?
    var mainbranch:MainBranch?
    var workspace:Workspace?
    var owner:Owner?
}

class RepoLinks:Codable {
    var watchers:RepoLinkReference?
    var branches:RepoLinkReference?
    var tags:RepoLinkReference?
    var commits:RepoLinkReference?
    var selfLink:RepoLinkReference?
    var source:RepoLinkReference?
    var avatar:RepoLinkReference?
    var html:RepoLinkReference?
    var hooks:RepoLinkReference?
    var forks:RepoLinkReference?
    var downloads:RepoLinkReference?
    var pullrequests:RepoLinkReference?
    var clone:[Clone]?
    enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case hooks, html, avatar, watchers, branches, tags, commits, source, forks, downloads, pullrequests, clone
    }
}

class Project:Codable {
    var links:RepoLinks?
    var type:String?
    var name:String?
    var key:String?
    var uuid:String?
}

/*class ProjectLinks:Codable {
    var selfLink:RepoLinkReference?
    var avatar:RepoLinkReference?
    var html:RepoLinkReference?
    enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case html, avatar
    }
}*/

class MainBranch:Codable {
    var type:String?
    var name:String?
}


class RepoLinkReference:Codable {
    var href:String?
}

class Clone: Codable {
    var href:String?
    var name:String?
}

class Workspace:Codable {
    var links:RepoLinks?
    var slug:String?
    var name:String?
    var type:String?
    var uuid:String?
}

class Owner:Codable {
    var links:RepoLinks?
    var username:String?
    var display_name:String?
    var type:String?
    var uuid:String?
}
