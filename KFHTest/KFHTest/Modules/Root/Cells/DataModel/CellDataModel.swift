//
//  CellDataModel.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class CellDataModel {
    var title:String
    var value:String
    
    init(title:String, value:String) {
        self.title = title
        self.value = value
    }
}
