//
//  SettingsTableViewCell.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(data:CellDataModel) {
        titleLabel.text = data.title
        valueLabel.text = data.value
        
        if let data = data as? SettingsDataModel {
            if data.isLogoutRow {
                backgroundColor = .lightGray
            }
            else {
                backgroundColor = .white
            }
        }
    }
}
