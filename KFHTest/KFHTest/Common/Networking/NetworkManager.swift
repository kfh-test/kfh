//
//  NetworkManager.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

enum HttpMethod:String{
    case get = "get"
    case post = "post"
}

class NetworkManager {
    
    private var authToken:String?
    
    static let shared = NetworkManager()
    
    func setCredentials(username:String, password:String) {
        authToken = "Basic \(Data("\(username):\(password)".utf8).base64EncodedString())"
    }
    
    func dataTask(serviceURL:String,httpMethod:HttpMethod,parameters:[String:String]?,completion:@escaping (AnyObject?, Error?, Int?) -> Void) -> Void {
        
        requestResource(serviceURL: serviceURL, httpMethod: httpMethod, parameters: parameters, completion: completion)
    }
    
    private func requestResource(serviceURL:String,httpMethod:HttpMethod,parameters:[String:String]?,completion:@escaping (AnyObject?, Error?, Int?) -> Void) -> Void {
        var request = URLRequest(url: URL(string:serviceURL)!)
        guard let authToken = authToken else {
            debugPrint("unable to login as credentials are not available")
            return
        }
        request.addValue(authToken, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        
        if (parameters != nil) {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
        }
        
        let sessionTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let response = response as? HTTPURLResponse {
                    if (data != nil && response.isResponseOK()){
                        completion (data as AnyObject, nil, response.statusCode)
                    }
                    else {
                        completion (nil, nil, response.statusCode)
                    }
                }
                
                if (error != nil) {
                    completion (nil, error!, nil)
                }
            }
        }
        sessionTask.resume()
    }
    
    func clearCache() {
        authToken = nil
    }
}
