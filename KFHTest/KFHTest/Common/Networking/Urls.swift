//
//  Urls.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

class Urls {
    static let WORKSPACE_KEY = "workspace"
    
    private static let BaseURL:String = "https://api.bitbucket.org/"
    private static let Version:String = "2.0"

    static var AUTHORIZE:String {
        get {
            return BaseURL + Version + "/user"
        }
    }
    
    static var GET_REPOSITORIES:String {
        get {
            var retVal = BaseURL + Version + "/repositories/"
            if let workspace = UserDefaults.standard.string(forKey: WORKSPACE_KEY) {
                retVal+=workspace
            }
            return retVal
        }
    }
}
