//
//  UITextField.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2020 ITS Mobile Banking. All rights reserved.
//

import UIKit

extension UITextField {
    
    var isEmpty:Bool {
        get {
            return self.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ?? false
        }
    }
    
    func clear() {
        self.text = ""
    }
}
