//
//  HTTPURLResponse.swift
//  KFHTest
//
//  Created by Deepanshu Bansal on 5/23/21.
//  Copyright © 2021 Deepanshu Bansal. All rights reserved.
//

import Foundation

extension HTTPURLResponse {
    func isResponseOK() -> Bool {
        return (200...299).contains(self.statusCode)
    }
}
